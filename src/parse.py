import yaml
import re
import csv
import collections
config = {}
config['tempora']={}
config['sancti']={}
fd = open('constants.py','r')
print(fd)

"""
This file generates config.yaml for calendar.

"""

import utils
parse = False 
for line in fd:
    if(line.startswith("#START")):
        print(line)
        parse = True
    elif(len(line) == 0 or line[0] == "#" or line[0] == '\n'):
        pass
    elif(parse):
        split_line_2 = ""
        name = None 
        split_line = line.split('#')
        if(len(split_line) > 1):
            fname = split_line[1].lstrip().rstrip('\n')
        else:
            fname = ""
        split_line_2 = split_line[0].split('=')
        idx = split_line_2[1].lstrip().rstrip('\n')

        idx2 = idx.replace("'", '').split(":",1)
        #print(idx2)
        feast = idx2[0].rstrip(' ')
        period = idx2[1].rstrip(' ')

        #ugly matches
        if(feast == "tempora"):
            #movable feasts
            match = re.match(r"([a-zA-Z]+)([0-9]+-[0-9a-zA-Z]+):([0-9a-zA-Z]+)", period, re.I)
            if match:
                items = match.groups()
                period2=items[0]
                if(period2 == "Pent"):
                    print("pentacost")
                weekday=items[1]
                misc=items[2]

                #very ugly parsing code
                try: 
                    temp = config[feast][period2]
                except KeyError:
                    config[feast][period2] = {}
                    
                try:                        
                    config[feast][period2][weekday]
                except KeyError:
                     config[feast][period2][weekday]={'rank':int(misc),'name':fname}
                     
                #print(config)
            #ember days
            match3 = re.match(r"([0-9]+-[0-9a-zA-Z]+):([0-9a-zA-Z]+)", period, re.I)
            if match3:
                items3 = match3.groups()
                #print(items3)
                weekday=items3[0]
                misc=items3[1]
                try:
                    temp = config[feast][weekday]
                except KeyError:
                    config[feast][weekday] = {'rank':int(misc),'name':str(fname)}
            

        elif(feast =="sancti"):
            #fixed feasts
            match2 = re.match(r"([0-9]+-[0-9a-zA-Z]+):([0-9a-zA-Z]+)", period, re.I)
            if match2:
                items2 = match2.groups()
                print(items2)
                weekday=items2[0]
                misc=items2[1]
                try:
                    temp = config[feast][weekday]
                except KeyError:
                    config[feast][weekday] = {'rank':int(misc),'name':str(fname)}
        
        else:
            print("no match")
            print(line)
empty =False
#parses the table of lessions
with open('../config/Table_LESSONS.csv', newline='') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
     for row in spamreader:

        #special case: treat all 3 rows as empty
        if(row[3] == row[4] and row[2] == row[3] and len(row[0]) > 0 ):
            print("Empty")
            #print(row[0])
            empty = True
            
        if(len(row[0])>0):
            #matches the id to movable feasts
            #print(row[0])
            item = utils.parse_idx_1(row[0])
            items=item[1]
            print(items)
            period2=items[0]
            weekday=items[1]
            if(not empty):
                try:
                    temp = config[feast][period2]
                except KeyError:
                    config[feast][period2] = {}                    
                    #print(row[0])
                y =  config["tempora"][period2][weekday]
                config["tempora"][period2][weekday]={**y,'mp_ot':row[2],'ep_ot_1':row[3],'ep_ot_2':row[4]}
        
        elif empty:
            print("empty row")
            #print(row)
            y =  config["tempora"][period2][weekday]
            print(y)
            if(row[1].startswith("First")):
                config["tempora"][period2][weekday]={**y,'mp_ot':row[2],'ep_ot_1':row[3],'ep_ot_2':row[4]}
                print( config["tempora"][period2][weekday])
            else:
                config["tempora"][period2][weekday]={**y, 'mp_nt':row[2],'ep_nt_1':row[3],'ep_nt_2':row[4]}
                print( config["tempora"][period2][weekday])
                empty =False

with open('../config/holy_day_psalms.csv', newline='') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
     for row in spamreader:
         idx = row[0]
         item = utils.parse_idx_1(idx)
         if(item[0] == "t0"):
            idxs=item[1]
            print(idxs)
            period2=idxs[0]
            weekday=idxs[1]
            #misc=idxs[2]
            y= config["tempora"][period2][weekday]
            config["tempora"][period2][weekday]={**y,'ps_mp':row[2],'ps_ep':row[3]}                           
         elif(item[0] == "t1"):
             idxs=item[1]
             period2=idxs[0]
             y=config["sancti"][day]
             config["tempora"][period2]={**y,'ps_mp':row[2],'ps_ep':row[3]}
         elif(item[0] == "t2"):
             idxs=item[1]
             day=idxs[0]
             y=config["sancti"][day]
             config["sancti"][day]={**y,'ps_mp':row[2],'ps_ep':row[3]}

         
print("config")
#print(config)
with open('../config/data.yml', 'w') as outfile:
    yaml.dump(config, outfile, default_flow_style=False)
