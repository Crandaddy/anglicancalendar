import yaml
from missal import MissalFactory
import logging
import re
import datetime,calendar
import utils
class AnglicanCalendar:
    ordinal = None
    feasts = None
    config = None
    missal = {}
    def __init__(self,config,year):
        self.config = config
        self.addYear(year)
        #self.calendar = missal.items()



    def ifChristmastide(self,date):
        if(date.month == 12 and (date.day >= 25 and date.day <= 31)):
            return True
        elif(date.month == 1 and (date.day >= 1 and date.day < 6)):
            return True
        else:
            return False

    def addYear(self,year):
        self.missal[year] = MissalFactory.create(year)

    def _getFeasts(self,date):
        return self.missal[date.year][date].getItems()

    def _parse_id_to_name(self,string):
        item = None
        try:            
            item = utils.parse_idx_1(string)
        except KeyError:
            raise KeyError("Index not found")
        if(item[0] == "t0"):
            idxs=item[1]
            period2=idxs[0]
            weekday=idxs[1]
            misc=idxs[2]
            item = self.config["tempora"][period2][weekday]
            return item
        elif(item[0] == "t1"):
            idxs=item[1]
            period2=idxs[0]
            item = self.config["tempora"][period2]
            return item
        elif(item[0] == "t2"):
            idxs=item[1]
            day=idxs[0]
            item = self.config["sancti"][day]
            return item
        raise KeyError
      
        
    def getDay(self,date):
        christmastide = self.ifChristmastide(date)
        try:
            item = self.missal[date.year][date].getItems()
        except KeyError:
            print("year not found")
            return
        #print(item)
        string = ""

        for x in range(len(item[0])):
            idx = str(item[0][x])
            time = idx.split(':',1)[0]
            print(time)

        string = "Season: "
        if(christmastide):

            string += "Christmastide"
        else:
            for x in range(len(item[0])):
                #parse ID
                idxs = utils.parse_idx_1(str(item[0][x]))
                print(idxs)
                string += utils._parse_period(idxs[1][0]) + " "
        string += "Feast: " 
        for x in range(len(item[1])):
            out2 = self._parse_id_to_name(str(item[1][x]))
            if(len(out2['name']) > 0):
                string += out2['name']
                if(x < len(item[1]) - 1 and len(item[1]) > 1 ):                        
                    string += " | "
        print(string)
        return string

    def getItemByDate(self,date):
        try:
            item = self.missal[date.year][date].getItems()
        except KeyError:
            print("year not found")
        print(item)
        item = self._parse_id_to_name(str(item[1][0]))
        return item 
    
    def getProperPsalms(self,date):
        item = self.getItemByDate(date)
        
        psstring = ""
        psstring += "Proper Psalms for " + str(date) + " are:\n"
        psstring += "Morning Prayer: " + item['ps_mp'] + "\n"
        psstring += "Evening Prayer: " +  item['ps_ep'] 
        return psstring
    
    def getProperOT(self,date):
        item = self.getItemByDate(date)
        
        string = ""
        string += "Proper Lessons for " + str(date) + " are:\nFirst Lesson\n"
        string += "Morning Prayer: " + item['mp_ot'] + "\n"
        string += "Evening Prayer: " +  item['ep_ot_1'] 
        string += " or " + item['ep_ot_2'] + "\n"
        return string
            
    def getProperNT(self,date):
        item = self.getItemByDate(date)
        
        epstring = ""
        epstring += "Second Lession:\n  "
        epstring += "Morning Prayer: " +  item['mp_nt'] + "\n"
        if(len( item['ep_nt_1'])> 0):
            epstring += "Evening Prayer: " + item['ep_nt_1'] + "\n"
        if(len( item['ep_nt_2']) > 0):
            epstring += " or " +item['ep_nt_2'] + "\n"            
        return epstring
        
    def getMonth(self,year,month):
        #number of days in a month
        try:
            num_days = calendar.monthrange(year, month)[1]
            days = [datetime.date(year, month, day) for day in range(1, num_days+1)]
            feasts = [self.getDay(day) for day in days]
        except:
            raise ValueError
        return [days,feasts]
    
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
    log = logging.getLogger(__name__)

    #sample run
    with open("../config/data.yml", 'r') as stream:
        missal = MissalFactory.create(2018)
        try:
            config = yaml.load(stream)
        except yaml.YAMLError as exc: 
            print(exc)
    
    anglican = AnglicanCalendar(config,2018)
    anglican.addYear(2019)
    anglican.addYear(2020)
    anglican.addYear(2021)
    anglican.addYear(2023)
    date = datetime.date(2018, 12, 25)

    array = anglican.getMonth(2018,9)
    for x in range(len(array[0])):
        print("{:s}: {:s}".format(str(array[0][x]),str(array[1][x])))

    print("lesson")
    try:
        print(anglican.getProperPsalms(date))
    except:
        print("Psalms not found")
    try:
        print(anglican.getProperOT(date))
    except:
        print("OT not found")
    try:
        print(anglican.getProperNT(date))
    except:
        print("NT not found")        
    """
    for k, v in missal.items():
        if k.weekday() == 6:
           log.info("---")
        log.info("%s %s %s", k.strftime('%A'), k,v)
    """
