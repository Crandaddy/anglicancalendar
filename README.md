An incomplete Anglican Calendar forked from
https://github.com/mmolenda/Missal1962
# Missal1962
1962 Roman Catholic Missal for the Traditional Latin Mass

* Calculate whole liturgical year
* Show Proprium Missae for selected day of the year
* Show Proprium Missae for selected liturgical day
